"""
TODO
----
- I'd like to change this to a class of it's own - not xr.DataArray

"""
import numpy as np
import xarray as xr
import pandas as pd
import sklearn
import pathlib
from . utils import grid_flattened_data


class GeoClusters(xr.Dataset):
    """
    Used to create and store cluster ensemble members.

    The following functions should be used:
        - load_cluster_netCDF:  import previously stored clusters
        - make_clusters_from_dataset: create clusters from a netCDF dataset
        - make_clusters_from_arrays: create clusters from variables columns
        - assign_cluster_labels: assign labels to points based on an existing
                cluster dataset
        - clusters: where cluster data is stored as an xarray.DataArray
    """
    __slots__ = ()

    @property
    def clusters(self):
        if 'clusters_flat' in self:
            xda = self.clusters_flat.unstack()
            xda.name = 'clusters'
            return xda
        else:
            print('You have not made or loaded clusters yet')

    data = clusters
    
    def load_cluster_netcdf(self, filename):
        assert isinstance(filename, (str, pathlib.Path))

        xda = xr.open_dataarray(filename)
        
        return self.load_cluster_dataarray(xda)
    
    def load_cluster_dataarray(self, xda):
        expected_dims = ['time', 'lat', 'lon']
        availabl_dims = set(xda.dims)
        cluster_dim_name = list(availabl_dims - set(expected_dims))
        if not cluster_dim_name:
            raise KeyError('You need at least one dimension in addition to'
                           ' [time, lat, lon]')

        dims = cluster_dim_name + list(expected_dims)
        xda = xda.transpose(*dims)
        xda = xda.rename({cluster_dim_name[0]: 'members'})

        self['clusters_flat'] = xda.stack(coords=expected_dims)

        uniq = np.unique(self.clusters_flat)
        self.attrs['n_clusters_per_member'] = uniq[~np.isnan(uniq)].size
        self.attrs['n_members'] = self.clusters_flat.shape[0]
        self.attrs['verbose'] = True
        self.attrs['n_jobs'] = -1

        if self.verbose:
            return self

    def make_clusters_from_arrays(self, time, lat, lon,
                                  *cluster_data_columns,
                                  n_members=25, n_clusters_per_member=16,
                                  max_features=None, n_jobs=-1, verbose=True,
                                  sname=None):
        """
        Create clusters from array-like columns and assign to clusters

        Parameters
        ----------
        time : array-like
            a column of times that are either month or day of the year
        lat : array-like
            column of gridded latitude values [-90 : 90]
        lon : array-like
            column of gridded longitude values [-180 : 180]
        cluster_data_columns: array-like
            any number of columns that match the length of time/lat/lon
            columns can be any continuous variable like temperature, salinity etc
        n_members : int [25]
            the number of ensemble members (number of times clustering will be
            repeated)
        n_clusters_per_member : int [16]
            the number of k-means cluster centers in each member
        max_features : int [None]
            selects the number of features from the dataset that will be used
            to cluster. Randomly selects a subset from the given features. 
            If None, then all variables will be used to cluster
        n_jobs : int [-1]
            number of parallel jobs. -1 uses all available cores
        verbose : bool/int [True]
            level of verbosity to pass to joblib (parallel package)
        sname : str [None]
            if string path is given, will save the data to the given path.
            If None, then data will not be saved, only assigned to
            GeoClusters.clusters
        """

        assert len(time) == len(lat), 'length of time and lat are not the same'
        assert len(time) == len(lon), 'length of time and lon are not the same'
        assert isinstance(n_members, int), 'n_members must be an intiger'
        assert isinstance(n_clusters_per_member, int), 'n_clusters_per_member must be an intiger'

        if max_features is None:
            max_features = np.array(cluster_data_columns).shape[1]

        self.attrs = dict(
            verbose=verbose,
            n_members=n_members,
            n_clusters_per_member=n_clusters_per_member,
            max_features=max_features,
            n_jobs=n_jobs)

        self._clusters_from_arrays(time, lat, lon, *cluster_data_columns,
                                   sname=sname)

        if self.verbose:
            return self

    def make_clusters_from_dataset(self, cluster_dataset, n_members=25,
                                   n_clusters_per_member=16, max_features=None,
                                   n_jobs=-1, verbose=True, sname=None):
        """
        Create clusters from an xarray.Dataset and assign to clusters

        Parameters
        ----------
        cluster_dataset: xarray.Dataset
            a netCDF with climatological data that contains clustering features
            the data must have the dimensions: time-like, lat, lon
            and time-like must be in either months or days of the year
        n_members : int [25]
            the number of ensemble members (number of times clustering will be
            repeated)
        n_clusters_per_member : int [16]
            the number of k-means cluster centers in each member
        max_features : int [None]
            selects the number of features from the dataset that will be used
            to cluster. Randomly selects a subset from the given features.
            If None, then all variables will be used to cluster
        n_jobs : int [-1]
            number of parallel jobs. -1 uses all available cores
        verbose : bool/int [True]
            level of verbosity to pass to joblib (parallel package)
        sname : str [None]
            if string path is given, will save the data to the given path.
            If None, then data will not be saved, only assigned to
            GeoClusters.clusters
        """

        if max_features is None:
            max_features = len(list(cluster_dataset.keys()))

        self.attrs = dict(
            verbose=verbose,
            n_members=n_members,
            n_clusters_per_member=n_clusters_per_member,
            max_features=max_features,
            n_jobs=n_jobs)

        keys = list(cluster_dataset.data_vars.keys())
        dims = list(cluster_dataset[keys[0]].dims)

        cluster_data_columns = [
            cluster_dataset[k].stack(coords=dims).to_series()
            for k in keys]
        coords = cluster_data_columns[0].reset_index()[dims]

        time = coords[dims[0]]
        lat = coords[dims[1]]
        lon = coords[dims[2]]

        self.attrs['cluster_vars'] = keys
        self._clusters_from_arrays(time, lat, lon, *cluster_data_columns,
                                   sname=sname)
        
        if self.verbose:
            return self

    def assign_cluster_labels(self, time, lat, lon):
        """
        Labels points from already trained clusters.
        Trained clusters have to be loaded for this function to work

        Parameters
        ----------
        time : array-like
            time in the same unit as the cluster time. Usually in month
            or in day of the year.
        lat : array-like
            latitude ranging from -90 to 90
        lon : array-like
            longitude ranging from -180 to 180

        Returns
        -------
        labels : array
            returns an array the same number of rows as time/lat/lon but
            with the same number of columns as the number of ensemble members.
        """

        def make_bins(a):
            if isinstance(a[0], np.datetime64):
                istime = True
                inf = np.timedelta64(10, 'Y').astype('timedelta64[ns]').astype(float)
            else:
                istime = False
                inf = np.inf

            a = a.astype(float) if istime else a

            a = np.convolve(a, [0.5]*2, 'valid')
            a = np.r_[a[0] - inf, a, a[-1] + inf]

            if istime:
                a = a.astype(float).astype('datetime64[ns]')
            return a

        if not hasattr(self, 'clusters_flat'):
            print('You have not loaded a cluster instance yet')
            return

        tvals = self.clusters.time.values
        yvals = self.clusters.lat.values
        xvals = self.clusters.lon.values

        bins = [make_bins(tvals),
                make_bins(yvals),
                make_bins(xvals)]

        labels = [np.arange(a.size - 1).astype(int) for a in bins]

        it = np.array(pd.cut(time, bins[0], labels=labels[0]), dtype=int)
        iy = np.array(pd.cut(lat,  bins[1], labels=labels[1]), dtype=int)
        ix = np.array(pd.cut(lon,  bins[2], labels=labels[2]), dtype=int)

        labels = self.clusters.values[:, it, iy, ix]

        return labels.T

    def _clusters_from_arrays(self, time, lat, lon, *cluster_data_columns,
                              sname=None):
        from sklearn import preprocessing
        import psutil
        import pathlib

        if sname is not None:
            self.attrs['sname'] = pathlib.Path(sname)
            self.attrs['sname_dir'] = self.sname.parent / (self.sname.name.split('.')[0] + '_models')
            self.attrs['sname_dir'].mkdir(exist_ok=True, parents=True)

        data_columns = self._array_1stdim_long(cluster_data_columns)
        assert data_columns.shape[0] == time.shape[0]

        if self.verbose:
            print(f'CLUSTERING: {self.n_members}x{self.n_clusters_per_member}')
            print('Scaling data: (x - avg) / std')
            print('Algorithm: sklearn.cluster.MiniBatchKmeans')
        nans = ~np.isnan(data_columns).any(1)
        data_nonans = data_columns[nans]
        # data_nonans_scaled = preprocessing.scale(data_nonans)
        data_nonans_scaled = data_nonans
        labels_nonans = np.ndarray([nans.size, self.n_members]) * np.nan

        n_jobs = psutil.cpu_count() if self.n_jobs < 1 else self.n_jobs
        n_jobs = self.n_members if (n_jobs > self.n_members) else n_jobs
        self.attrs['n_jobs'] = n_jobs

        if self.verbose:
            print(f'Clustering {self.n_members} members with '
                  f'{n_jobs} parallel jobs')
        output = self._parallel(
            self._cluster_data,
            [data_nonans_scaled] * self.n_members,
            n_jobs=n_jobs,
            verbose=self.verbose)
        labels, models = zip(*output)
        labels = np.concatenate(labels, axis=1)

        labels_nonans[nans] = labels

        xda = grid_flattened_data(*labels_nonans.T, time=time, lat=lat,
                                  lon=lon, return_dataaray=True)
        xda = xda.rename({'columns': 'members'})
        xda = xda.stack(coords=['time', 'lat', 'lon'])
        self['clusters_flat'] = xda

        if sname is not None:
            import joblib
            for i, model in enumerate(models):
                model_sname = self.sname_dir / f"{self.sname.name.split('.')[0]}_{i:02d}.joblib_pkl"
                joblib.dump(model, model_sname)
            xdb = self.clusters
            xdb.attrs['n_ensemble_members'] = self.n_members
            xdb.attrs['n_clusters_per_member'] = self.n_clusters_per_member
            xdb.attrs['cluster_vars'] = str(self.attrs['cluster_vars'])
            # xdb.attrs['cluster_setup'] = str(self.model)
            xdb.to_netcdf(sname)
            if self.verbose:
                print(f'Clustering instance has been saved to {sname}')

    def _cluster_data(self, data_nonans):
        from sklearn import mixture, cluster
        
        f = self.max_features - 1
        k = self.n_clusters_per_member
        cols = np.arange(data_nonans.shape[1])
        subset = np.random.choice(cols[1:], f, replace=False)
        subset = np.r_[cols[0], subset]
        
        model = cluster.MiniBatchKMeans(
            n_clusters=k, 
            verbose=0,
            max_iter=80,
            max_no_improvement=7,
            n_init=1)
        self.attrs['model'] = str(model)
        labels = model.fit_predict(data_nonans[:, subset])

        return labels[:, None], model

    def _parallel(self, func, *args, n_jobs=-1, verbose=1):
        from joblib import Parallel, delayed

        lengths = set([len(a) for a in args])
        assert len(lengths) == 1, 'All parallel inputs must have the same length on the 1st dimension'

        func = delayed(func)
        proc = Parallel(backend='loky', n_jobs=n_jobs, verbose=verbose)

        out = proc(func(*a) for a in zip(*args))

        return out

    def __repr__(self):
        import re
        txt = xr.Dataset.__repr__(self)
        spacer = len(re.findall(r'(.*)(\(\w*\))', txt)[0][0]) - len('Cluster:') - 2

        txt = re.sub(r"data variables:\s*clusters_flat", 'cluster:' + ' '*spacer, txt, flags=10)
        txt = re.sub("Attributes", "clustering options", txt, flags=10)
        return txt

    @staticmethod
    def _array_1stdim_long(a):
        a = np.array(a)
        shape = a.shape

        assert len(shape) <= 2, 'input arrays can have maximum of two dimensions'

        if shape[0] > shape[1]:
            return a
        else:
            return a.T

