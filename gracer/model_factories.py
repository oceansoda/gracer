

def OLS(X, *args, **kwargs):
    from sklearn.linear_model import LinearRegression
    
    return LinearRegression()


def Lasso2(X, *args, **kwargs):
    from sklearn.linear_model import Lasso
    from sklearn.preprocessing import PolynomialFeatures
    from sklearn.pipeline import make_pipeline
    
    model = make_pipeline(
        PolynomialFeatures(2),
        Lasso(alpha=1),
    )
    
    return model


def GBDT(X, *args, **kwargs):
    from lightgbm import LGBMRegressor
    N = X.shape[0]
    model = LGBMRegressor(
        objective='l2',
        extra_trees=True,
        max_bin=500,
        min_child_samples=int(N**0.5), 
        num_leaves=int(N**0.5),
        learning_rate=.2,
        importance_type='gain',
        reg_alpha=20,
        reg_lambda=20,
        n_estimators=500,
        path_smooth=50,
        n_jobs=1,
        verbose=0,
    )
    
    return model


def SVR(X, *args, **kwargs):
    import sklearn.svm
    from sklearn.model_selection import RandomizedSearchCV
    from sklearn.utils.fixes import loguniform
    import numpy as np
    
    max_N = 4000
    N = X.shape[0]
    n = max_N if N > max_N else N
    
    def make_train_test_subset():
        s = n//4
        idx = np.random.randint(0, N, n)
        tst, trn = idx[:s], idx[s:]
        return trn, tst

    subsets = [
        make_train_test_subset(),
        make_train_test_subset(),
        make_train_test_subset(),
        make_train_test_subset(),
    ]
    
    params = {
        'C': loguniform(1e0, 8e2), 
        'gamma': loguniform(1e-3, 5e-1),
        'nu': [0.1, 0.2, 0.4], 
    }

    model = RandomizedSearchCV(
        sklearn.svm.NuSVR(verbose=0, kernel='rbf'), 
        params, 
        n_iter=100,
        n_jobs=4, 
        verbose=1, 
        cv=subsets,
    )
    return model


def NuSVR(x, y):
    import numpy as np
    from sklearn import metrics
    from sklearn import model_selection
    from sklearn import svm
    from sklearn import preprocessing

    r = int(1 + (y.size // 4000))

    n_split = np.min([5, y.size])
    kfolds = [i for i in model_selection.KFold(n_splits=n_split).split(x[::r], y[::r])]

    cv = model_selection.GridSearchCV(
        estimator=svm.NuSVR(kernel='rbf'),
        param_grid={
            'gamma': [0.01, 0.1, 1],
            'C': [350, 100, 35, 10],
            'nu': [0.2, 0.4]},
        cv=kfolds,
        verbose=1,
        n_jobs=20)

    cv.fit(x[::r], y[::r])

    estimator = cv.best_estimator_
    r = int(1 + (y.size // 8000))
    estimator.fit(x[::r], y[::r])

    estimator.parameters = str({
        'C': estimator.C,
        'gamma': estimator.gamma,
        'nu': estimator.nu,
        'SVs': estimator.support_.size
    })
    
    print(estimator.parameters)
    return estimator


def FFNN_SK(x, y):
    import numpy as np
    from sklearn import neural_network, model_selection

    r = int(1 + (y.size // 5000))
    f = y.size // 30
    hidden_layer1 =  [1 + int(y.size**0.55)]

    n_split = np.min([5, y.size])
    kfolds = [i for i in model_selection.KFold(n_splits=n_split).split(x[::r], y[::r])]
    
    from joblib import Parallel, delayed, parallel_backend
    
    
    
    with parallel_backend("loky"):
        model = neural_network.MLPRegressor(
            solver='lbfgs' if y.size < 1500 else 'adam',
            batch_size='auto', learning_rate='adaptive',
            shuffle=True, validation_fraction=0.333,
            early_stopping=True, n_iter_no_change=10, max_iter=10000)

        cv = model_selection.GridSearchCV(
            refit=False,
            estimator=model,
            param_grid={
            "alpha": [0.001, 0.1, 10, 1000, 100000],
            "hidden_layer_sizes": np.r_[np.meshgrid(hidden_layer1, [20])].reshape(2, -1).T.tolist()},
            cv=kfolds,
            verbose=6,
            n_jobs=25)

        cv.fit(x[::r], y[::r])

    params = dict(batch_size='auto', max_iter=5000, early_stopping=True, shuffle=False, verbose=False)
    params.update(cv.best_params_)
    print(y.size, model.solver, params)
    model.set_params(**params)

    model.fit(x, y)

    model.parameters = str({
        'hidden_layer_size': model.hidden_layer_sizes,
        'batch_size': model.batch_size,
        'alphaL2': model.alpha,
    })

    return model


def FFNN_Keras(X, y, *args, **kwargs):
#     import os
#     os.environ['KERAS_BACKEND'] = 'theano'
    import keras as ks
    
    from keras import Sequential
    from keras.activations import relu
    from keras.layers import Dense, Dropout
    from keras.callbacks import EarlyStopping
    from keras.wrappers.scikit_learn import KerasRegressor
    from keras.regularizers import l1_l2
    from keras.optimizers import Adadelta, Adam, Nadam, RMSprop
    from keras.losses import MeanSquaredError
    
    n_train_points, n_features = X.shape
    hidden_layer_size = 1 + int(n_train_points**0.6)
    
    model = Sequential([
        Dense(hidden_layer_size, 
              activation=relu, 
              bias_regularizer=l1_l2(0.1, 0.4),
              kernel_regularizer=l1_l2(0.1, 0.4),
              input_shape=[n_features]),
        Dropout(0.1), 
        Dense(1)])

    model.compile(
        optimizer=Adam(lr=0.2), 
        loss=MeanSquaredError())
    
    return model
