import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import xarray as xr
import pandas as pd
from .. import metrics


def plot_ts_ml_diagnostic(y, yhat, station='', variable='', color='C0'):
    """
    Assess a time series with various metrics
    
    Returns
    -------
    fig: figure object
    ax: a list of axes objects
    """
    resid = yhat - y
    grp = resid.to_xarray().groupby('time.month')
    clim = grp.mean('time')
    upper = clim + grp.std('time')
    lower = clim - grp.std('time')
    avg = y.mean()

    fig = plt.figure(figsize=[8.5, 4.5], dpi=120)
    ax = np.array([
        plt.subplot2grid([2, 3], [0, 0], 1, 2, fig=fig),
        plt.subplot2grid([2, 3], [0, 2], 1, 1, fig=fig),
        plt.subplot2grid([2, 3], [1, 0], 1, 1, fig=fig),
        plt.subplot2grid([2, 3], [1, 1], 1, 1, fig=fig),
        plt.subplot2grid([2, 3], [1, 2], 1, 1, fig=fig),
    ])

    x = y.index.values
    ax[0].plot(x, y, color='#CCCCCC', label='Observed')
    ax[0].plot(x, yhat, color=color, label='Predicted')
    ax[0].legend(loc=0, frameon=0, ncol=2)

    sns.kdeplot(y, color='#CCCCCC', ax=ax[1], vertical=True, legend=False)
    sns.kdeplot(yhat, color=color, ax=ax[1], vertical=True, legend=False)
    ax[1].set_ylim(ax[0].get_ylim())

    sns.regplot(y, yhat, ax=ax[2], scatter_kws=dict(s=15, color=color), fit_reg=False)
    ax[2].plot(ax[0].get_ylim(), ax[0].get_ylim(), ls=':', c='k', lw=1.5)
    ax[2].set_xlim(ax[0].get_ylim())
    ax[2].set_ylim(ax[0].get_ylim())

    sns.residplot(y, yhat, ax=ax[3], scatter_kws=dict(s=15, color=color))
    ax[3].set_xlim(ax[0].get_ylim())
    ax[3].set_yticklabels([])

    clim.plot(ax=ax[4], lw=3, color=color)
    ax[4].fill_between(clim.month, upper, lower, color=color, alpha=0.4)
    ax[4].axhline(0, color='k', linestyle=':', lw=1.5, zorder=0)
    ax[4].set_ylim(ax[3].get_ylim())
    ax[4].set_xticks(range(2, 12, 2))
    
    [a.set_ylabel('') for a in ax]
    [a.set_xlabel('') for a in ax]
    [a.set_ylabel(variable) for a in ax[[0, 1, 2]]]
    [a.set_xlabel(variable) for a in ax[[2, 3]]]
    [a.yaxis.set_label_position("right") for a in ax[[1, 4]]]
    [a.yaxis.tick_right() for a in ax[[1, 3, 4]]]
    
    ax[4].set_ylabel(f'Resid. {variable}')
    ax[4].set_xlabel('Month')
    
    [[s.set_color('#CCCCCC') for s in a.spines.values()] for a in ax]
    
    metrics_dict = get_metrics(y, yhat, ['count', 'bias', 'huber', 'rmse', 'r2_score'])
    txt = ''
    for k, v in metrics_dict.items():
        v = int(v) if (v//1) == v else f"{v:.2g}"
        txt += f"{k}: {v}\n"
    ax[2].text(0.95, 0.03, txt[:-1], va='bottom', ha='right', transform=ax[2].transAxes)
    
    p = ax[0].get_position()
    fig.text(0.5, p.y1 + 0.02, f'{station}: time series prediction analysis - {variable}', va='bottom', ha='center')

    return fig, ax


def time_series_comparison(y, yhat, no_data='ignore', return_metrics=True, colocate_tolerance=0.25):
    """
    y, yhat: xarray.DataArray
        must have time, lat, lon as dimensions for colocation
    no_data can be:
        ignore - will make plot anyway and return what it can
        fail - will fail if there is no data
        pass - will not make a figure and return None
    """
    import pandas as pd
    
    def has_coords(xda):
        dims = [k for k in xda.dims]
        hascoords = np.isin(['time', 'lat', 'lon'], dims).all()
        assert hascoords, 'must have time lat lon'
    
    has_coords(y)
    has_coords(yhat)
    
    i = y.notnull().squeeze().values
    t0, t1 = pd.DatetimeIndex([y.time.values[i].min(), y.time.values[i].max()])
    
    yhat = yhat.sel(lon=y.lon, lat=y.lat, tolerance=colocate_tolerance, method='nearest')
    yhat = yhat.sel(time=slice(t0, t1))
    y = y.squeeze().sel(time=slice(t0, t1))
    
    station = getattr(y, 'station', '').values
    variable = getattr(y, 'name', '')
        
    if no_data == 'fail':
        assert y.count() > 0, 'there is no data in `y`'
        assert yhat.count() > 0, 'there is no data in `yhat`'
    elif no_data == 'pass':
        return 
    else:
        pass
    
    y = y.squeeze().to_series()
    yhat = yhat.squeeze().to_series()
    
    if return_metrics:
        results = get_metrics(y, yhat, metrics.listed),
    else:
        results = None,
    
    return plot_ts_ml_diagnostic(y, yhat, station, variable) + results


def gridded_data_comparison(y, yhat, test_mask=None, var='', unit=''):
    from warnings import filterwarnings
    from astropy import convolution as conv
    from numpy import sqrt, square
    from cartopy import crs
    import ocean_data_tools as odt
    from matplotlib import cm
    import matplotlib as mpl
    
    filterwarnings('ignore', category=RuntimeWarning)
    
    if test_mask is None:
        test_mask = y.copy()
        test_mask = (test_mask * 0 + 1).astype(bool).rename('test')
    coords = dict(lat=np.arange(-89.5, 90), lon=np.arange(-179.5, 180))
    resid = yhat - y
    resid_train = resid.where(~test_mask).reindex(**coords)
    resid_test = resid.where(test_mask).reindex(**coords)
    
    # DIRECT TIME SERIES
    ts_yhat = yhat.where(y.notnull()).mean(['lat', 'lon'])
    ts_y = y.mean(['lat', 'lon'])
    
    # TIME SERIES DATA
    annual_bias = resid.resample(time='1AS').mean(['time', 'lat', 'lon'])
    annual_rmse = sqrt(square(resid).mean(['lat', 'lon'])).resample(time='1AS').mean('time')
    annual_rmse_test = annual_rmse.where(test_mask.resample(time='1AS').all(test_mask.dims))

    # MAPS DATA 
    kernel = conv.Gaussian2DKernel(1)
    map_bias = resid_test.mean('time').convolve(kernel)
    map_rmse = sqrt(square(resid_test).mean('time')).convolve(kernel)
    nobs = map_bias.count()
    if nobs < 5000:
        step = 4
    elif nobs < 10000:
        step = 2
    else: 
        step = 1
    map_bias = map_bias.coarsen(lat=step, lon=step).mean()
    map_rmse = map_rmse.coarsen(lat=step, lon=step).mean()
    
    # LAT PLOTS DATA
    mjjaso = resid.time.dt.month.isin((5, 6, 7, 8, 9, 10))
    ndjfma = resid.time.dt.month.isin((11, 12, 1, 2, 3, 4))
    def roll_lat5(xda): return xda.rolling(lat=5, center=True).mean()
    merid_bias_JJA = roll_lat5(            resid_test .where(mjjaso).mean(['lon', 'time']))
    merid_bias_DJF = roll_lat5(            resid_test .where(ndjfma).mean(['lon', 'time']))
    merid_rmse_JJA = roll_lat5(sqrt(square(resid_test).where(mjjaso).mean(['lon', 'time'])))
    merid_rmse_DJF = roll_lat5(sqrt(square(resid_test).where(ndjfma).mean(['lon', 'time'])))

    # FLATTENED DATA
    y_, yhat_, istest = (
        xr.merge([y, yhat, test_mask])
        .to_dataframe()
        .dropna()
        .values
        .astype(float)
        .T)
    istest = istest.astype(bool)
    
    metrics_list = ['count', 'bias', 'mae', 'huber', 'rmse', 'r2_score']
    df = pd.DataFrame.from_dict({
        'Train': get_metrics(y_[~istest], yhat_[~istest], metrics_list),
        'Test': get_metrics(y_[istest], yhat_[istest], metrics_list)})
    text = df.round(2).to_markdown()
    
    # function is only for sanity - keep it seperated from data processing
    def make_plot():
        from matplotlib import pyplot as plt

        fig = plt.figure(figsize=[8.5, 12.5], dpi=120)

        shape = [17, 7]
        map_props = dict(projection=crs.PlateCarree(-155))
        ax = np.array([
            plt.subplot2grid(shape, [3,  0], 3, 4, fig=fig),  # time-series
            plt.subplot2grid(shape, [3,  4], 3, 3, fig=fig),  # text
            plt.subplot2grid(shape, [6,  0], 4, 5, fig=fig, **map_props),  # map-bias
            plt.subplot2grid(shape, [6,  5], 4, 2, fig=fig),  # lat-bias
            plt.subplot2grid(shape, [10, 0], 4, 5, fig=fig, **map_props),  # map-rmse
            plt.subplot2grid(shape, [10, 5], 4, 2, fig=fig),  # lat-rmse
            plt.subplot2grid(shape, [14, 0], 3, 3, fig=fig),  # resid
            plt.subplot2grid(shape, [14, 3], 3, 2, fig=fig),  # y-y (test)
            plt.subplot2grid(shape, [14, 5], 3, 2, fig=fig),  # y-y (train)
            plt.subplot2grid(shape, [ 0, 0], 3, 7, fig=fig),  # y-y (train)
        ])
        ax = np.r_[ax, ax[9].twinx()]
    
        fig.subplots_adjust(hspace=2, wspace=0.4, top=0.95, bottom=0.02, left=0.05, right=0.95)
        
        ####################
        ## DIRECT DATA TS ##
        ts_yhat.plot(ax=ax[9], zorder=3, label='Estimated')
        ts_y.plot(   ax=ax[9], zorder=2, label='Observed', color='k', lw=2)
        ax[10].fill_between(y.time.values, y.count(['lat', 'lon']), color='#CCCCCC')
        
        ax[ 9].legend(loc='lower right', ncol=2, frameon=0)
        ax[ 9].set_zorder(2)
        ax[ 9].set_facecolor('none') 
        ax[ 9].set_ylabel(f'{var} ({unit})')
        ax[ 9].set_xticks(ax[9].get_xticks())
        ax[10].grid(axis='x', which='major', lw=0.8, color='w', )
        ax[10].set_ylabel('Number of obs.')
        ax[10].set_zorder(1)
        [s.set_color('#CCCCCC') for s in ax[ 9].spines.values()]
        [s.set_color('#CCCCCC') for s in ax[10].spines.values()]
        
        ##################
        ## SUMMARY TEXT ##
        ##################

        ax[1].text(0.5, 0.5, text, ha='center', va='center', size=10, family='monospace')
        ax[1].set_frame_on(False)
        ax[1].set_xticks([])
        ax[1].set_yticks([])
        
        #################
        ## Time series ##
        #################
        props = dict(width=0.8)
        annual_rmse     .to_series().plot.bar(**props, ax=ax[0], color='#BBBBBB', label='RMSE')
        annual_bias     .to_series().plot.bar(**props, ax=ax[0], color='k', label='Bias', zorder=5)
        annual_rmse_test.to_series().plot.bar(**props, ax=ax[0], color='C0', label='Testing')

        ticks = np.arange(0, annual_bias.size, 5)
        odt.plotting.pimp_plot(ax[0], color='w', lw=1)
        ax[0].set_xticks(ticks)
        ax[0].set_xticklabels([f'{y+1985}' for y in ticks], rotation=0)
        ax[0].legend(loc=0, ncol=4, frameon=0)
        ax[0].set_ylabel(f'{var} ({unit})')
        
        ##########
        ## MAPS ##
        ##########
        props = dict(add_colorbar=0, rasterized=True)
        map_bias.plot_map(**props, ax=ax[2], levels=np.arange(-25, 25.1, 2.5), cmap=cm.RdBu_r)
        map_rmse.plot_map(**props, ax=ax[4], levels=np.arange(  0, 35.1, 2.5), cmap=cm.Reds)
        cbars = [make_cbar(a, loc=[-0.035, 0, 0.03, 1], extend='neither') for a in ax[[2, 4]]]
        ax[2].text(90, 50, 'Test bias', transform=crs.PlateCarree(), ha='center', zorder=6)
        ax[4].text(90, 50, 'Test RMSE', transform=crs.PlateCarree(), ha='center', zorder=6)
        cbars[0].set_label(f'{var} bias ({unit})')
        cbars[1].set_label(f'{var} RMSE ({unit})')

        ###############
        ## Lat plots ##
        ###############
        lat = merid_bias_JJA.lat.values
        ax[3].plot(merid_bias_JJA, lat, label='JJA')
        ax[3].plot(merid_bias_DJF, lat, label='DJF')
        ax[5].plot(merid_rmse_JJA, lat, label='JJA')
        ax[5].plot(merid_rmse_DJF, lat, label='DJF')
        
        lims = np.array([ax[3].get_xlim(), ax[5].get_xlim()])
        lims = np.min(lims), np.max(lims)
        [a.set_xlim(lims)                          for a in ax[[3, 5]]]
        [a.set_ylim(-90, 90)                       for a in ax[[3, 5]]]
        [odt.plotting.pimp_plot(a)                 for a in ax[[3, 5]]]
        [a.axvline(0, color='k', lw=0.5, zorder=0) for a in ax[[3, 5]]]
        ax[3].legend(loc='lower right', frameon=0)
        ax[3].set_xlabel(f'{var} bias ({unit})')
        ax[5].set_xlabel(f'{var} RMSE ({unit})')

        ###################
        ## RESIDUAL PLOT ##
        ###################
        props = dict(cmap=cm.CMRmap_r, norm=mpl.colors.LogNorm(), rasterized=True)
        ax[6].axhline(0, color='k', lw=0.8, ls='--')
        ax[6].hexbin(y_[ istest], yhat_[ istest] - y_[istest], **props)
        ax[7].hexbin(y_[ istest], yhat_[ istest], **props)
        ax[8].hexbin(y_[~istest], yhat_[~istest], **props)
        
        ax[6].text(0.05, 0.9, 'Test residuals', va='bottom', ha='left', transform=ax[6].transAxes)
        ax[7].text(0.1, 0.9, 'Test data ', va='bottom', ha='left', transform=ax[7].transAxes)
        ax[8].text(0.1, 0.9, 'Train data', va='bottom', ha='left', transform=ax[8].transAxes)
        [a.set_xlim(np.nanpercentile(y_, [0.01, 99.99]))   for a in ax[6:9]]
        [a.grid('both', color='#CCCCCC', ls='--', lw=0.8)  for a in ax[6:9]]
        [odt.plotting.pimp_plot(a, lw=0)                   for a in ax[6:9]]
        [a.set_xlabel(f'Obs. {var} ({unit})')              for a in ax[6:9]]
        [a.set_ylim(np.nanpercentile(y_, [0.01, 99.99]))   for a in ax[7:9]]
        [a.yaxis.tick_right()                              for a in ax[7:9]]
        [a.yaxis.set_label_position("right")               for a in ax[7:9]]
        [a.plot(a.get_xlim(), a.get_ylim(), '--k', lw=0.8) for a in ax[7:9]]
        ax[6].set_ylim(np.nanpercentile(yhat_ - y_, [0.01, 99.99]))
        ax[6].set_ylabel(f'Residual ({unit})')
        ax[7].set_yticklabels([])
        ax[8].set_ylabel(f'Est. {var} ({unit})')

        return fig, ax

    return make_plot()


def make_cbar(ax, loc=[0, -0.05, 1, 0.05], **kwargs):
        mappable = ax.get_children()[0]
        p = ax.get_position()


        x0 = p.x0 + p.width * loc[0]
        y0 = p.y0 + p.height * loc[1]
        w = p.width * loc[2]
        h = p.height * loc[3]

        fig = ax.get_figure()
        cax = fig.add_axes([x0, y0, w, h])

        if loc[2] > loc[3]:
            kwargs.update({'orientation': 'horizontal'})

        cbar = plt.colorbar(
            mappable=mappable,
            cax=cax, 
            **kwargs)

        if loc[0] < 1:
            cax.yaxis.set_ticks_position('left')
            cax.yaxis.set_label_position('left')

        cbar.outline.set_linewidth(0)
        ax.outline_patch.set_lw(0)

        return cbar

    
def get_metrics(y, yhat, metric_names):
    return {m: getattr(metrics, m)(y, yhat) for m in metric_names}
