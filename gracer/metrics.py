from numpy import abs, array, nanmean, nanmedian, nanstd, isfinite, nan


def gracer_metric(func):
    def inner(*args, **kwargs):
        args = [array(a) for a in args]
        return func(*args, **kwargs)
    return inner


@gracer_metric
def count(y, y_hat):
    return y.size


@gracer_metric
def bias(y, y_hat):
    return nanmean(y_hat - y)


@gracer_metric
def mae(y, y_hat):
    return nanmean(abs(y_hat - y))


@gracer_metric
def huber(y, y_hat, delta=1.35):
    from scipy.special import huber
    return nanmean(huber(delta, y_hat - y))


@gracer_metric
def rmse(y, y_hat):
    return nanmean((y_hat - y)**2)**0.5


@gracer_metric
def r2_score(y, y_hat):
    from sklearn.metrics import r2_score
    i = isfinite([y, y_hat]).all(0)
    if i.sum() > 2:
        return r2_score(y[i], y_hat[i])
    else:
        return nan


@gracer_metric
def stdev(y, y_hat):
    return nanstd(y)


@gracer_metric
def mape(y, y_hat): 
    return nanmean(abs((y - y_hat) / y)) * 100


listed = []
local = list(locals().items())
for key, value in local:

    if callable(value) and getattr(value, '__module__', '') == __name__:
        listed += key, 
listed.remove('gracer_metric')


def check_metric(metric):
    raise_err = False
    if metric == 'all':
        metric = listed
    elif isinstance(metric, list):
        metric = metric
    elif isinstance(metric, str):
        if metric in listed:
            metric = [metric]
        else:
            raise_err = True
    else:
        raise_err = True

    if raise_err:
        raise TypeError(
            'metrics must be `all` or a list of metric names with any of '
            'the following: ' +str(metrics.listed))
    
    return metric
