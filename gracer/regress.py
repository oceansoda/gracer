import numpy as np
import xarray as xr
import pandas as pd
import sklearn
import pathlib
from sklearn.base import RegressorMixin
# from sklearn.linear_model._base import LinearModel
from sklearn import linear_model
from . import metrics
from functools import wraps

from warnings import filterwarnings
filterwarnings('once', category=UserWarning)


class GRaCER(RegressorMixin):
    """
    Geospatial Random Cluster Ensemble Regressor

    GRaCER is a regression framework that first clusters data based on
    continous variables (e.g. temperature, salinity, etc) and then performs
    a regression for each cluster. The process is repeated several times
    to form an ensemble of estimates. The mean of all estimates is taken
    for the final estimate.
    """

    def __init__(
        self, 
        clusters_fname=None, 
        regression_factory=None, 
        n_jobs=-1, 
        verbose=True
    ):
        """
        Geospatial Random Cluster Ensemble Regressor

        GRaCER is a regression framework that first clusters data based on
        continous variables (e.g. temperature, salinity, etc) and then performs
        a regression for each cluster. The process is repeated several times
        to form an ensemble of estimates. The mean of all estimates is taken
        for the final estimate. 

        Parameters
        ----------
        clusters_fname : string, pathlib.Path
            the file path of the netCDF file containing clusters from
            GeoClusters output.
        regressor : instantiated regression class
            a regression object that has fit and predict methods like
            the sklearn api.
        n_jobs : int
            the number of parallel processes that will be run.
            Note that parallel only works as a threaded process for now
        verbose : bool / int
            the level of verbosity passed to joblib
        """
        from . clustering import GeoClusters
        self.n_jobs = n_jobs
        self.regression_factory = regression_factory
        self.verbose = verbose

        if clusters_fname is None:
            print('No precomputed clusters filename provided - '
                  'you will need to calculate clusters with '
                  'ClusterEnsembleRegressor.make_clusters')
            
        elif isinstance(clusters_fname, str):
            clusters_fname = pathlib.Path(clusters_fname)
            if clusters_fname.is_file():
                self.clusters = GeoClusters()
                self.clusters.load_cluster_netcdf(clusters_fname)
                self.assign_cluster_labels = self.clusters.assign_cluster_labels
                if self.verbose:
                    print(f'Cluster dataset: {clusters_fname}')
            else:
                raise FileNotFoundError(
                    f'The given string ({clusters_fname}) is not a file')
        elif isinstance(clusters_fname, xr.DataArray):
            self.clusters = GeoClusters()
            self.clusters.load_cluster_dataarray(clusters_fname)
            self.assign_cluster_labels = self.clusters.assign_cluster_labels
        else:
            raise TypeError(
                'Keyword argument `clusters_fname` must be a string path')

        if regression_factory is None:
            from . model_factories import OLS
            if self.verbose:
                print('sklearn.linear_model.LinearRegression will be used '
                      'as regressor')
            self.regression_factory = OLS
        elif self.verbose:
            print(f'Regressor: {self.regression_factory}')

    def make_clusters(self, cluster_dataset, n_members=25, 
                      n_clusters_per_member=16, sname=None):
        """
        Create clusters from an xarray.Dataset and assign to clusters
        (a wrapper for GeoClusters.make_clusters_from_dataset)

        Parameters
        ----------
        cluster_dataset: xarray.Dataset
            a netCDF with climatological data that contains clustering features
            the data must have the dimensions: time-like, lat, lon
            and time-like must be in either months or days of the year
        n_members : int [25]
            the number of ensemble members (number of times clustering will be
            repeated)
        n_clusters_per_member : int [16]
            the number of k-means cluster centers in each member
        max_features : int [None]
            selects the number of features from the dataset that will be used
            to cluster. Randomly selects a subset from the given features.
            If None, then all variables will be used to cluster
        n_jobs : int [-1]
            number of parallel jobs. -1 uses all available cores
        verbose : bool/int [True]
            level of verbosity to pass to joblib (parallel package)
        sname : str [None]
            if string path is given, will save the data to the given path.
            If None, then data will not be saved, only assigned to 
            GeoClusters.clusters

        Returns
        -------
        clusters : None
            does not return anything, but adds the cluster object under
            GRaCER.clusters, as well as GRaCER.assign_cluster_labels that
            is used to get cluster labels for regression fitting and 
            predicting.
        """
        self.clusters = GeoClusters()
        self.clusters.make_clusters_from_dataset(cluster_dataset, n_members,
                                                 n_clusters_per_member,
                                                 sname=sname,
                                                 verbose=self.verbose)
        self.assign_cluster_labels = self.clusters.assign_cluster_labels

    def fit(self, X, y, clusters=None, **kwargs):
        """
        Trains the regression model.

        Parameters
        ----------
        X : array-like
            a column-wise array with predictor-features as the columns.
            May not contain any nans.
        y : array-like
            the target variable that may not contain nans
        clusters : array-like
            a column-wise array that has the same number of rows as
            X and y. The number of columns represents the size of the
            ensemble.
        validation_indicies: bool-array
            Boolean array that has the same shape as y, that indicates what 
            inputs should be kept asside as validation data. This is done so
            that validation is cluster sensitive. 

        Returns
        -------
        a trained instance of GRaCER
        """
        from sklearn.preprocessing import StandardScaler
        
        if hasattr(X, 'columns'):
            self.predict_cols = X.columns
        else:
            raise UserWarning('X must be a dataframe with a name')
        if hasattr(y, 'name'):
            self.target_col = y.name
        else:
            raise UserWarning('y must be a series with a name')
        
        if clusters is None:
            return self._fit_easy(X, y, **kwargs)
        
        clusters = self._array_1stdim_long(np.array(clusters, ndmin=2))
        X = self._array_1stdim_long(np.array(X, ndmin=2))
        y = np.array(y).squeeze()
        
        if 'sample_weight' in kwargs:
            if kwargs['sample_weight'] is None:
                kwargs['sample_weight'] = np.ones_like(y)

        n_estimators = clusters.shape[1]
        estimators = [ClusterRegressor(self.regression_factory)
                      for _ in range(n_estimators)]

        inputs = [X] * n_estimators, [y] * n_estimators, clusters.T, estimators
        func = lambda x, y, c, est: est.fit(x, y, c, **kwargs)
        
        self.estimators_ = self._parallel(func, *inputs) 

        return self
    
    def predict(self, X, clusters=None, reduce_func=[np.nanmean], skip_nans=False):
        """
        Predicts for values in X.

        Parameters
        ----------
        X : array-like
            a column-wise array with predictor-features as the columns.
            May not contain any nans.
        clusters : array-like
            a column-wise array that has the same number of rows as
            X and y. The number of columns represents the size of the
            ensemble.
        reduce_func : callable
            a function that reduces the ensemble values to a single value.
            must take `axis` as a keyword argument. Recommended that you
            use nan-aware function (just in case). Can be used to get the
            standard deviation, or non-nan count for each cluster.
        skip_nans : bool
            some regressors cannot handle nans. This allows you to with nans 
            without the need to remove the nans prior to prediction. 

        Returns
        -------
        an estimate of y_hat for the given variables
        """
        from warnings import filterwarnings
        filterwarnings('ignore', '.*Mean of empty slice.*')

        if isinstance(X, (pd.DataFrame, pd.Series)):
            index = X.index
        else:
            index = np.arange(X.shape[0])
        
        if reduce_func is not None:
            assert isinstance(reduce_func, (list, tuple, callable)), (
                'The reduce_func must be a callable, list of callables, or None')
            if isinstance(reduce_func, (list, tuple)):
                assert all([callable(f) for f in reduce_func]), (
                    'All entries in reduce_func must be callable')
            elif isinstance(reduce_func, callable):
                reduce_func = [reduce_func]
        
        if clusters is None:
            kwargs = dict(reduce_func=reduce_func, skip_nans=skip_nans)
            return self._predict_easy(X, **kwargs)
            
        clusters = self._array_1stdim_long(np.array(clusters, ndmin=2))
        X = self._array_1stdim_long(np.array(X, ndmin=2))

        n_estimators = len(self.estimators_)
        assert n_estimators == clusters.shape[1], (
            'There must be the same number of clusters as estimators')

        inputs = [X] * n_estimators, clusters.T, self.estimators_
        func = lambda x, c, est: est.predict(x, c, skip_nans=skip_nans)
        out = self._parallel(func, *inputs)

        # if the reduce_func is None then columns will not be combined
        if reduce_func is None:
            return pd.DataFrame(np.array(out).T, index=index).astype(float)

        # if the reduce_func is a list of callables (asserted earlier), then a 
        # DataFrame will be returned, where the column name is the function __name__
        combined = pd.DataFrame(columns=[f.__name__ for f in reduce_func], index=index)
        for f in reduce_func:
            column = f(out, axis=0).astype(float)
            name = f.__name__
            combined.loc[:, name] = pd.Series(column, index=index)
        
        return combined
    
    def _fit_easy(self, X, y, **kwargs):
        col1 = X.columns[0]
        coords = X[col1].reset_index().drop(columns=col1).values.T
        
        clusters = self.assign_cluster_labels(*coords)
        
        return self.fit(X, y, clusters, **kwargs)
    
    def _predict_easy(self, X, **kwargs):
        """
        Same as predict, but it is assumed that X is a dataframe with cluster
        coords as index columns
        """
        col1 = X.columns[0]
        coords = X[col1].reset_index().drop(columns=col1).values.T
        
        clusters = self.assign_cluster_labels(*coords)
        
        out = self.predict(X, clusters, **kwargs)
        return out

    def _parallel(self, func, *args):
        from joblib import Parallel, delayed, parallel_backend

        lengths = set([len(a) for a in args])
        assert len(lengths) == 1, 'All parallel inputs must have the same length on the 1st dimension'

        func = delayed(func)
        
        with parallel_backend("threading"):
            proc = Parallel(
                n_jobs=self.n_jobs,
                verbose=self.verbose)
            out = proc(func(*a) for a in zip(*args))

        return out

    @staticmethod
    def _array_1stdim_long(a):
        shape = a.shape

        assert len(shape) <= 2, 'input arrays can have maximum of two dimensions'

        if shape[0] > shape[1]:
            return a
        else:
            return a.T

    def get_deep_regress_attribute(self, attr_name_or_func, labels=None, agg_func='mean'):
        """
        Fetches an attribute from the regressor-per-cluster level
        and returns the mean. Note that 1D vectors can also be fetched.
        Output is averaged across cluster members. 
        """
        from itertools import product

        if callable(attr_name_or_func):
            get_deep_attr = attr_name_or_func
            attr_name = attr_name_or_func.__name__
        elif isinstance(attr_name_or_func, str):
            get_deep_attr = lambda o: getattr(o, attr_name_or_func)
            attr_name = attr_name_or_func
        else:
            raise TypeError('attr_name_or_func must be a string or function')

        def get_regressor_prop(m, k):
            members = self.estimators_[m].regressors
            if k not in members:
                return None
            else:
                regressor = members[k]
                return get_deep_attr(regressor)
            
        clusters = self.clusters.clusters_flat
        
        n_members = clusters.n_ensemble_members
        n_clusters = clusters.n_clusters_per_member
        iterator = lambda: product(range(n_members), range(n_clusters))
        
        for m, c in iterator():
            attr = get_regressor_prop(m, c)
            if attr is not None:
                break

        if np.iterable(attr):
            size = len(attr)
        else:
            size = 1
        coords = list(range(size)) if labels is None else labels

        output = (
            xr.concat(objs=[clusters.copy()] * size, dim=attr_name)
            .transpose('members', 'coords', attr_name)
            .assign_coords(**{attr_name: coords})
        )
        
        for m, c in iterator():
            attr = get_regressor_prop(m, c)
            if attr is None:
                continue
            
            idx = clusters.values[m] == c
            output.values[m, idx, :] = attr
                
        output = output.unstack().squeeze()
        output = getattr(output, agg_func)(dim='members')
        output.name = attr_name

        return output
    
    def cluster_scores(self, y, y_hat, y_clusters, metric='all', agg_func='mean', verbose=True):
        assert hasattr(self, 'estimators_'), 'You have not trained the model yet'
        clusters = self.clusters.clusters_flat
        metric = metrics.check_metric(metric)
        
        n_members = clusters.n_ensemble_members
        n_clusters = clusters.n_clusters_per_member
        n_metrics = len(metric)    
    
        output = (
            xr.concat(objs=[clusters.copy() * np.nan] * n_metrics, dim='metric')
            .transpose('members', 'coords', 'metric')) 
        output['metric'] = metric
        
        # get results for each member
        for m, clus_reg in enumerate(self.estimators_):
            if verbose: 
                print(m, end=' ')
            mclus = y_clusters[:, m]
            results = clus_reg.cluster_scores(y, y_hat, mclus, metric)

            # assign for each cluster
            for c in range(n_clusters):
                if c not in results.index:
                    continue
                
                idx = clusters.values[m] == c
                output.values[m, idx, :] = results.loc[c]
                
        output = output.unstack().squeeze()
        output = getattr(output, agg_func)(dim='members')
        output.name = 'regression_metrics'

        return output
    
    @property
    def name(self):
        n_members = self.clusters.n_members
        n_clusters = self.clusters.n_clusters_per_member
        regressor = self.regression_factory.__name__
        target = self.target_col.split('_')[0]
        
        return f"GRaCE_{target}_m{n_members}k{n_clusters}_{regressor}"
    
    def save(self, save_path='../models/', suffix='', extension='gzip'):
        """
        Save the GRaCER model as a compressed pickle file. 
        Use gracer.GRaCER.load to load the file
        
        save_path: str
            if an existing folder is given the name will be generated from the
            properties in GRaCER (recommended). If the direct parent of the 
            given path exists, then will write to that exact given path. 
        suffix: str
            suffix will be added just before the file extension when the name
            is generated automactically. 
        """
        import  pickle
        from pathlib import Path
        
        name = self.name + suffix
        path = Path(save_path)
        
        if path.is_dir():
            save_path = path / name
        elif (not path.is_dir()) & path.parent.is_dir():
            save_path = path
        elif path.is_file():
            raise FileExistsError(f'File already exists: {path}')
        else:
            raise FileNotFoundError(
                'save_path must be either an existing folder or the parent must '
                'exist. ')
        if save_path.is_file():
            raise FileExistsError(f'File already exists: {save_path}')
        
        path = str(save_path)
        with open(path, 'wb') as file:
            pickle.dump(self, file)#, compression='gzip')
        
        return path
    
    @staticmethod
    def load(path, compression='infer'):
        """
        path: str
            where the model is saved (pickle file)
        compression: str
            'gzip', 'zip', 'infer'
        """
        import pickle
        
        with open(path, 'rb') as file:
            model = pickle.load(file)
        
        return model
        

class ClusterRegressor(RegressorMixin):
    def __init__(self, regressor_factory, warn_unfitted_cluster=False):
        if regressor_factory is None:    
            from . model_factories import OLS
            self.regressor_factory = OLS

        self.regressor_factory = regressor_factory
        self.warn_unfitted_cluster = warn_unfitted_cluster

    def fit(self, X, y, clusters, **kwargs):
        from warnings import warn
        from sklearn.preprocessing import StandardScaler
        from sklearn.base import clone
        import copy
        import time

        self.n_clusters = clusters.max() + 1
        self.regressors = {}        
        self.scalers = {}
        cluster_data = self._cluster_data_generator(clusters)

        validation_idx = kwargs.pop('validation_indicies', np.zeros_like(y).astype(bool))
        training_idx = ~validation_idx
        assert validation_idx.size == y.size, 'validation_indicies must be the same size as y'
        if validation_idx.sum() == 0:
            use_validation = False
        else: 
            use_validation = True
        
        for c, i in cluster_data:
            if c < 0:  # skip nans
                continue 
                
            train = i & training_idx
            valid = i & validation_idx
            
            if train.sum() < 40:
                continue
            tx, ty = X[train], y[train]
            self.scalers[c] = StandardScaler()
            tx = self.scalers[c].fit_transform(tx)
                        
            if use_validation:
                if valid.sum() <= 2:
                    continue
                vx, vy = X[valid], y[valid]
                vx = self.scalers[c].transform(vx)
                kwargs.update({'validation_data': (vx, vy)})
            
            # creating model now that we know we'll run it
            model = self.regressor_factory(tx, ty)
            model.n_training_points = train.sum()
            model.n_validation_points = valid.sum()
            
            # catch for LightGBM 
            if 'early_stopping_rounds' in kwargs:
                kwargs['eval_set'] = kwargs.pop('validation_data', None)
            
            try:
                model.predict(tx[:1])
            except:
                model.fit(tx, ty, **kwargs)
            
            self.regressors[c] = model

        return self

    def predict(self, X, clusters, skip_nans=False):

        self._check_fitted(clusters)

        yhat = np.ndarray(clusters.shape) * np.nan
        cluster_data = self._cluster_data_generator(clusters)
        # prefix c denotes cluster
        for c, i in cluster_data:
            if c not in self.regressors:
                continue
            cx = X[i]
            if skip_nans:
                nans = np.isnan(cx).any(1).squeeze()
                cx[nans, :] = 0
            else: 
                nans = np.zeros(cx.shape[0]).astype(bool).squeeze()
            
            cx = self.scalers[c].transform(cx)

            cyhat = self.regressors[c].predict(cx).squeeze()
            cyhat[nans] = np.nan
            yhat[i] = cyhat

        return yhat

    def score(self, X, y, clusters, metric='all'):
        
        yhat = self.predict(X, clusters)
        summary = self.cluster_scores(y, yhat, clusters, metric=metric)

        return summary

    def cluster_scores(self, y, yhat, clusters, metric=['rmse']):
        
        metric = metrics.check_metric(metric)
        
        y = y.squeeze()
        yhat = yhat.squeeze()
        summary = pd.DataFrame()

        cluster_generator = [['TOTAL', np.isfinite(y)]]
        if clusters is not None:
            cluster_generator += list(self._cluster_data_generator(clusters))

        for c, i in cluster_generator:
            if i.sum() <= 1:
                continue
            cy = y[i]
            cyhat = yhat[i]
            nans = np.isnan([cy, cyhat]).any(0)
            
            for key in metric:
                func = getattr(metrics, key)
                if nans.all():
                    summary.loc[c, key] = np.NaN
                else:
                    summary.loc[c, key] = func(cy[~nans], cyhat[~nans])
        
        if -999 in summary.index:
            summary = summary.drop(-999)
        return summary
    
    def _cluster_data_generator(self, clusters):
        clusters[np.isnan(clusters)] = -999
        list_of_clusters = np.unique(clusters).astype(int)

        for c in list_of_clusters:
            i = (clusters == c)
            yield c, i

    def _check_fitted(self, clusters=None):

        if not hasattr(self, 'regressors'):
            raise NotImplementedError('Model has not been trained yet. use the .fit method')

        if clusters is not None:
            clusters[np.isnan(clusters)] = -999
            input_clusters = set(np.unique(clusters))
            train_clusters = set(list(self.regressors.keys()))
            untrained_clusters = input_clusters - train_clusters

            if len(untrained_clusters) > 0:
                if len(untrained_clusters) == 1:
                    untrained_clusters = [int(tuple(untrained_clusters)[0])]
                else:
                    untrained_clusters = [int(u) for u in
                                          tuple(untrained_clusters)]

                not_trained_area = np.c_[[clusters == c for c in
                                          untrained_clusters]].sum(0)
                num_untrained = not_trained_area.sum()
                tot_area = not_trained_area.size

                msg = ('\nClusterRegressor has untrained clusters: '
                       f'{untrained_clusters} where '
                       f'the untrained clusters are {num_untrained:d} of '
                       f'{tot_area:d} total pixels')
                if self.warn_unfitted_cluster:
                    print(msg)


def quantile_subset(y):
    n = y.size

    fracs = [0, 0.05, 0.125, 0.25, 0.5, 0.75, 0.875, 0.95, 1]
    sorti = np.argsort(y)

    idx = []
    for c in range(len(fracs) - 1):
        i0 = int(fracs[c+0] * n)
        i1 = int(fracs[c+1] * n)
        quant = sorti[i0:i1]
        np.random.shuffle(quant)
        idx += quant[:n//20],

    i = np.concatenate(idx)
    return i
