# from seaflux import gas_transfer_CO2 as kw
# from PyCO2SYS.api import CO2SYS_wrap as co2sys


def train_config(**config):
    import xarray as xr
    import numpy as np
    from . import GRaCER, model_factories
    
    print('Loading and preparing data')
    data = xr.open_zarr(config['data_file'])
    
    x = config['x']
    y = config['y']
    drop = config.get('nandropcols', None)
    
    df = data[x + [y]].load().to_dataframe().reset_index()
    df = df.dropna(subset=drop)

    Y = df[y]
    X = df[x].set_index([df.time.dt.month, df.lat, df.lon])
    s = df.time.dt.year.isin(config['testing_years']).values
    v = df.time.dt.year.isin(config['validation_years']).values
    t = ~(s | v)
    
    if isinstance(config['model_factory'], str):
        model_factory = getattr(model_factories, config['model_factory']) 
    elif callable(config['model_factory']):
        model_factory = config['model_factory']
    else:
        raise TypeError('config["model_factory"] should be callable or a string of function in gracer.model_factories')
    print('Fitting')
    model = GRaCER(
        clusters_fname=config['cluster_file'],
        regression_factory=model_factory,
        n_jobs=config['model_njobs'],
        verbose=config.get('verbose', 1))

    model.fit(
        X, Y, 
        validation_indicies=v,
        **config['fitting_settings'])
    
    if config.get('summary_figure', False):
        from . utils import grid_flattened_data
        from . diagnostics import gridded_data_comparison
        coords = dict(time=df.time, lat=df.lat, lon=df.lon)
        YHAT = model.predict(X).nanmean
        def gridit(df): 
            xda = grid_flattened_data(df, **coords).squeeze().drop('columns')
            return xda
        YHAT = gridit(YHAT).rename('yhat')
        Y = gridit(Y).rename('y')
        TEST = gridit(~t).astype(bool).rename('mask')
        fig, ax = gridded_data_comparison(Y, YHAT, TEST, y, )
        title = (
            f"{config['name']} training stats\n{config['y']}: " + 
            str(np.array(config['x'])).replace("'", '').replace(' ', ', ')[1:-1]
        )
        fig.suptitle(title, va='bottom', y=0.965)
    
    try:
        if 'savename' in config:
            model.save(config['savename'])
    except:
        pass
    finally:
        return model
    

def predict(model, data, delta_ATM_name='', skip_nans=False, n_jobs=1, verbose=False):
    from warnings import filterwarnings
    from ocean_data_tools import sparse 
    from numpy import nanmean, nanstd, nanmedian
    import xarray as xr
    import pandas as pd
    
    filterwarnings('ignore', category=RuntimeWarning)
    filterwarnings('ignore', '.*performance.*')
    
    model.verbose = verbose
    model.n_jobs = n_jobs
    
    x = model.predict_cols
    y = model.target_col
    
    if isinstance(data, xr.Dataset):
        df = data[x].to_dataframe().reset_index()
    elif isinstance(data, pd.DataFrame):
        df = data[x].reset_index()
    else:
        raise TypeError('data must be DataArray or DataFrmae')
    
    time = df.time
    month = time.dt.month
    lat = df.lat
    lon = df.lon
    
    predicted = model.predict(
        df.set_index([month, lat, lon])[x], 
        reduce_func=[nanmean, nanmedian, nanstd], 
        skip_nans=skip_nans)
    
    df[y + '_predAvg'] = predicted.nanmean.values
    df[y + '_predStd'] = predicted.nanstd.values
    df = df.set_index([time, lat, lon])
    
    df_pred_only = df[[y + '_predAvg', y + '_predStd']]
    
    xds = xr.Dataset.from_dataframe(df_pred_only, sparse=True)
    import ocean_data_tools as odt
    xds = odt.sparse.asdense(xds)
    
    if delta_ATM_name in data:
        xds['pco2_predAvg'] = xds[y + '_predAvg'] + data[delta_ATM_name]
        xds.pco2_predAvg.attrs = {
            'description': y + '_predAvg + ' + delta_ATM_name,
            'long_name': 'pCO$_2$',
            'units': '$\mu$atm',
        }
    
    return xds


def get_model_metrics(model, data, metrics='all'):
    
    x = model.predict_cols.values.tolist()
    y = model.target_col
    i = [i for i, n in enumerate(x) if n == 'sss_glodap']
    if len(i) == 1:
        x[i[0]] = 'sss_soda'
        
    df = data[x + [y]].to_dataframe().reset_index()
    df = df.dropna(subset=x+[y])

    Y = df[y]
    X = df[x].set_index([df.time.dt.month, df.lat, df.lon])
    
    CLUS = model.assign_cluster_labels(df.time.dt.month, df.lat, df.lon)
    YHAT = model.predict(X)
    
    scores = model.cluster_scores(Y.values, YHAT.values, CLUS, metric=metrics)
    
    return scores


def error_propagation(
    talk, pco2, temp, salt, si, po4,
    talk_predict_err=0, 
    pco2_predict_err=0,
    talk_measure_err=0,
    pco2_measure_err=0,
    talk_represent_err=0,
    pco2_represent_err=0,
    temp_uncert=0,
    salt_uncert=0,
    sio4_uncert=0,
    po4_uncert=0,
    verbose=False,
    **kwargs
):
    import numpy as np
    import xarray as xr
    import PyCO2SYS as pyco2
    from warnings import filterwarnings
    filterwarnings('ignore', category=RuntimeWarning)
    
    if verbose:
        print('Calculating CO2SYS')
    xds = co2sys(
        pco2=pco2.clip(1, 5000),
        alk=talk.clip(1, 5000),
        temp_in=temp,
        sal=salt,
        si=si,
        po4=po4,
        K1K2_constants=10,
        verbose=False)
    if isinstance(xds, xr.Dataset):
        df = xds.to_dataframe().dropna()
    else:
        df = xds
    co2dict = {k: np.array(df[k].values) for k in df}

    def combine_uncertainties(*args):
        return np.mean([a**2 for a in args], axis=0)**0.5
    
    talk_total_uncert = combine_uncertainties(
        talk_predict_err, 
        talk_represent_err, 
        talk_measure_err)
    
    pco2_total_uncert = combine_uncertainties(
        pco2_predict_err, 
        pco2_represent_err, 
        pco2_measure_err)
    
    calc_uncert_for = ['TCO2', 'pHinTOTAL']
    uncert_inputs = {
        "PAR1": talk_total_uncert,
        "PAR2": pco2_total_uncert,
        "TEMPIN": temp_uncert,
        "SAL": salt_uncert,
        "SI": sio4_uncert,
        "PO4": po4_uncert}
    
    if verbose:
        print('Propagating uncertainties CO2SYS')
    uncert, components = pyco2.uncertainty.propagate(
        co2dict, 
        calc_uncert_for, 
        uncert_inputs, **kwargs)
    
    return uncert, components


def error_prop_contribution(
    talk, pco2, temp, salt, si, po4,
    talk_measure_err,
    pco2_measure_err,
    talk_represent_err,
    pco2_represent_err,
    talk_predict_err, 
    pco2_predict_err,
    temp_uncert=0.65,
    salt_uncert=0.2,
    sio4_uncert=4,
    po4_uncert=0.1,
    **kwargs
):
    import pandas as pd
    
    avgs = talk, pco2, temp, salt, si, po4,
    
    uncertainty_rotations = dict(
        other = dict(
            temp_uncert=temp_uncert, salt_uncert=salt_uncert, 
            sio4_uncert=sio4_uncert, po4_uncert=po4_uncert),
        measurement = dict(
            talk_measure_err=talk_measure_err,
            pco2_measure_err=pco2_measure_err),
        representation = dict(
            talk_represent_err=talk_represent_err,
            pco2_represent_err=pco2_represent_err),
        prediction = dict(
            talk_predict_err=talk_predict_err,
            pco2_predict_err=pco2_predict_err))
    
    all_uncertainties = {}
    for rotation_key in uncertainty_rotations:
        rotation = uncertainty_rotations[rotation_key]
        for key in rotation:
            all_uncertainties[key] = rotation[key]
            
    outputs = pd.DataFrame()
    for key in uncertainty_rotations:
        print(key)
        outputs[key] = pd.DataFrame(error_propagation(*avgs, **uncertainty_rotations[key])[0]).iloc[0]
    outputs['total'] = pd.DataFrame(error_propagation(*avgs, **all_uncertainties         )[0]).iloc[0]
    
    def avg_uncert(*args):
        import numpy as np
        return np.mean([a**2 for a in args], axis=0)**0.5
    outputs.loc['TAlk', 'measurement'] = talk_measure_err
    outputs.loc['TAlk', 'representation'] = talk_represent_err
    outputs.loc['TAlk', 'prediction'] = talk_predict_err
    outputs.loc['TAlk', 'total'] = avg_uncert(talk_measure_err, talk_represent_err, talk_predict_err)
    outputs.loc['pCO2in', 'measurement'] = pco2_measure_err
    outputs.loc['pCO2in', 'representation'] = pco2_represent_err
    outputs.loc['pCO2in', 'prediction'] = pco2_predict_err
    outputs.loc['pCO2in', 'total'] = avg_uncert(pco2_measure_err, pco2_represent_err, pco2_predict_err)
    
    return outputs