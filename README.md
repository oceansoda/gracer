Geospatial Random Cluster Ensemble Regression
=============================================

GRaCER is a package that predicts ocean variables using a two-step cluster-regression approach. 


## Importing 

```python

import site
site.addsitedir('parent_path/GRaCER')

import gracer
```


## Running the code

This can be separated into three parts:

1. Create the clusters from seasonal climatologies of predictor variables (e.g., climatologies of SST, salinity, chlorophyll)
2. Train a regressor for each cluster using some sort of regressor (some examples provided)
3. Perform inference, i.e., do the mapping of the target variable

### Clusters

One of the features of the GRaCER method is that it creates multiple realisations of clusters. Each of these clusters are slightly different due to the random nature of the KMeans clustering algorithm, combined with the fact that we are clustering continuous variables that do not have a clearly defined boundary. 

This results in an ensemble of cluster-regression members. Averaging over these members decreases the overfitting due to data distribution within each cluster. Further, it has the added benefit of reducing the appearance of discrete boundaries. 

There is no hard and fast rule on how many members or clusters you should choose. This is up to you to decide. 

```python
def load_cluster_data() -> xr.Dataset:
    """
    A function that returns an xr.Dataset with the variables 
    being each of the predictors that will be used. 
    
    The dimensions need to be [time, lat, lon] - if you have month or 
    dayofyear, rename them to time. 
    """
    ...
    return data


data = load_cluster_data()

# create a cluster object that we will use to train
clusters = gracer.clustering.GeoClusters()
clusters = clusters.make_clusters_from_dataset(
    data, 
    n_members=35,  # number of ensemble members that will be estimated
    n_clusters_per_member=12,  # number of clusters in each member
    n_jobs=35,  # parallel processes - do not set to more than ncpus or n_members
    # if sname is given, the output will be saved to the specified file name if not returns xr.DataArray
    sname='./clusters/alk_clusters_nonorm_n35c12_doy.nc')
```

### Training the regressors

The GRaCER package provids you with a tool to do the regression per cluster for you! 

```python
def load_training_data() -> (pd.DataFrame, pd.Series):
    """
    A function that returns x, y training data. 
    It's up to you to do the train-test split. 
    The output should contain features (x) and target (y). 
    N.B. Either x, or y should have (time, lat, lon) indicies
    """
    ...
    return x, y

# load our data from the function above
df_x, df_y = load_training_data()

# create a GRaCER regression class that we'll interact with
model = gracer.regress.GRaCER(
    # notice that this file is the name from the clustring step
    clusters_fname='./clusters/alk_clusters_nonorm_n35c12_doy.nc',
    # this determines the regressor type for each cluster - more on this below
    regression_factory=gracer.model_factories.SVR)

# we have to assign clusters to the observations based on the time, lat, lon
# we use the function below to make this easier - time unit defines the time unit of the clusters (i.e., month, daysofyear)
time, lat, lon = gracer.utils.get_coords_from_dataframe(df_x, time_unit='daysofyear')
# since we load the model with the cluster file, we can directly use the model to assign cluster labels based on time, lat, lon (output is a numpy array)
arr_c = model.assign_cluster_labels(time, lat, lon)

# now we do the fitting
model.fit(df_x, df_y, arr_c)

# once the model is fit, you need to test it
yhat = model.predict(df_x)

... # assess model performance with metrics

# we save the object as a joblib pickle file
model.save('./models/alk_gracer-svr-n35c12.joblib_pkl')
# you have to load the gracer package to be able to load this saved file. 
```

In the code above, the `gracer.regress.GRaCER` function requires the cluster file name from the previous step, and it requires a `regress_factory`. The `regress_factory` is a function that returns an `sklearn` model that is already trained with the correct hyperparameters, or that will be trained with called with `model.fit(x, y)`, for example `sklearn.model_selection.RandomizedSearchCV`. In the `regress_factory`, you need to specify what hyperparameters you want to search across. See `gracer.model_factories` for examples. 

The great thing about the `GRaCER` class is that it will automatically do the regression for each cluster in each ensemble member. 


### Inference

Once the model trained, tested, and saved, you can do some inference! 

Note that if inference is computationally expensive for your model, then this might take ages to predict since you are doing this for multiple clusters (even though inference is often done in parallel.)

```python
import joblib
import gracer  # you need this to import the model

def load_inference_features(time_step) -> pd.DataFrame:
    """
    Loads data that is passed to the model, where the columns
    in the output of the function match the columns of (x) used
    to train the model.
    """
    ...  # do some things
    return df_x


def save_output(df_y, time_step):
    """
    function to save your output
    """
    # create a name to save your data with
    sname = "output_data_{t:04d}.dat".format(t=time_step)

    ... # do some things to format df_y into the desired format (e.g. netCDF)

    y.save(sname)
    

# load the model from the previous step with joblib
model = joblib.load('./models/alk_gracer-svr-n35c12.joblib_pkl')

# here we iterate through a bunch of time steps for which we do inference for each
for time_step in list_of_times:
    # load the prediction data only
    df_x = load_inference_features(time_step)

    # get the cluster labels
    time, lat, lon = gracer.utils.get_coords_from_dataframe(df_x, time_unit='daysofyear')
    arr_c = model.assign_cluster_labels(time, lat, lon)

    # and do the predictions 
    # skip_nans allows nans to be present in the features - simply skipped
    df_yhat = model.predict(df_x, arr_c, skip_nans=True)

    # finally, save the ouptut
    save_output(df_yhat, time_step)
```
